// We import the CSS which is extracted to its own file by esbuild.
// Remove this line if you add a your own CSS build pipeline (e.g postcss).
// import "../css/app.css"

// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
// import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "./vendor/some-package.js"
//
// Alternatively, you can `npm install some-package` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
import "./livesocket"
import lolight from "lolight"
import tagger from '@jcubic/tagger'

document.addEventListener("DOMContentLoaded", () => {
	lolight("pre code")
	darkMode()
	tagInput()
})


function darkMode() {
	document.querySelectorAll(".theme-toggle").forEach(toggle => {
		toggle.addEventListener('click', e => {
			e.preventDefault()

			const theme = toggle.dataset.theme
			document.documentElement.dataset.theme = theme
			localStorage.theme = theme
		})
	})
}

function tagInput() {
	document.querySelectorAll('.tag-input').forEach(el => {
		tagger(el, {
			add_on_blur: true,
			completion: {
				list: function (tags) {
					return window.fetch("/api/v1/notes/tags").then(resp => {
						return resp.json()
					})
				}
			},
			wrap: true,
			link: () => false
		})
	})
}


