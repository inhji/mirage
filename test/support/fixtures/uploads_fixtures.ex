defmodule Mirage.UploadsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Mirage.Uploads` context.
  """

  @doc """
  Generate a upload.
  """
  def upload_fixture(attrs \\ %{}) do
    {:ok, upload} =
      attrs
      |> Enum.into(%{
        filename: "some filename",
        title: "some title"
      })
      |> Mirage.Uploads.create_upload()

    upload
  end
end
