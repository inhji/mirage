defmodule Mirage.Repo.Migrations.AddSvgImageToList do
  use Ecto.Migration

  def change do
    alter table(:lists) do
      add :svg_image, :text
    end
  end
end
