defmodule Mirage.Repo.Migrations.AddMissingWikiListId do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add_if_not_exists :wiki_list_id, references(:lists, on_delete: :nothing, type: :binary_id)
    end
  end
end
