defmodule Mirage.Repo.Migrations.RemoveSiteSettingsFromUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :motd
      remove :custom_css
    end
  end
end
