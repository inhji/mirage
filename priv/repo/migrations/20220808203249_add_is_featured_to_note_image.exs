defmodule Mirage.Repo.Migrations.AddIsFeaturedToNoteImage do
  use Ecto.Migration

  def change do
    alter table(:notes_images) do
      add :is_featured, :boolean, default: false
    end
  end
end
