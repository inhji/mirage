defmodule Mirage.Repo.Migrations.ChangeListContentToText do
  use Ecto.Migration

  def change do
    alter table(:lists) do
      modify :content, :text, from: :string
      modify :content_html, :text, from: :string
    end
  end
end
