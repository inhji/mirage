defmodule Mirage.Repo.Migrations.CreateSettings do
  use Ecto.Migration

  def change do
    create table(:settings, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :value, :text
      add :notes, :text

      timestamps()
    end

    create unique_index(:settings, :name)
  end
end
