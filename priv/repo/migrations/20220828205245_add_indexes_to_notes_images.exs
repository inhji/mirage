defmodule Mirage.Repo.Migrations.AddIndexesToNotesImages do
  use Ecto.Migration

  def change do
    create unique_index(:notes_images, [:slug])
    create unique_index(:notes_images, [:title])
  end
end
