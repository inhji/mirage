defmodule Mirage.Repo.Migrations.CreateUploads do
  use Ecto.Migration

  def change do
    create table(:uploads, primary_key: false) do
      add(:id, :binary_id, primary_key: true)
      add(:title, :string)
      add(:image_filename, :string)

      timestamps()
    end
  end
end
