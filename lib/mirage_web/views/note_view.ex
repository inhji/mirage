defmodule MirageWeb.NoteView do
  use MirageWeb, :view

  def syndication_url(%{url: nil}), do: "#"
  def syndication_url(%{url: url}), do: url

  def syndication_text(%{type: :mastodon}), do: "Mastodon"
  def syndication_text(%{type: _}), do: ""

  def has_syndication?(%{url: nil}), do: false
  def has_syndication?(%{url: _}), do: true

  def featured_images(note) do
    Enum.filter(note.images, fn i -> i.is_featured == true end)
  end

  def note_date(note) do
    note.published_at || note.inserted_at
  end

  def image_link(image, version \\ :thumb) do
    Mirage.Notes.NoteUploader.url({image.filename, image}, version)
  end
end
