defmodule MirageWeb.LayoutView do
  use MirageWeb, :view

  # Phoenix LiveDashboard is available only in development by default,
  # so we instruct Elixir to not warn if the dashboard route is missing.
  @compile {:no_warn_undefined, {Routes, :live_dashboard_path, 2}}

  def motd(nil), do: "YMMV"
  def motd(""), do: motd(nil)

  def motd(motd_text) do
    motd_text
  end

  def route_name(conn) do
    controller_name =
      conn.private.phoenix_controller
      |> Module.split()
      |> List.last()
      |> String.replace_suffix("Controller", "")
      |> String.downcase()

    action_name = conn.private.phoenix_action

    "#{controller_name}-#{action_name}"
  end
end
