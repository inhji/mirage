defmodule MirageWeb.ActivityPub.ActorView do
  use MirageWeb, :view

  @context [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1"
  ]

  def render("actor.json", %{user: user}) do
    actor_url = Routes.activity_pub_actor_url(MirageWeb.Endpoint, :actor)
    inbox_url = Routes.activity_pub_actor_url(MirageWeb.Endpoint, :inbox)

    %{
      "@context" => @context,
      "id" => actor_url,
      "type" => "Person",
      "preferredUsername" => user.handle,
      "inbox" => inbox_url,
      "publicKey" => public_key(user, actor_url)
    }
  end

  def render("inbox.json", _params) do
    %{}
  end

  defp public_key(user, actor_url) do
    %{
      "id" => actor_url <> "#main-key",
      "owner" => actor_url,
      "publicKeyPem" => user.pub_key
    }
  end
end
