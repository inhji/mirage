defmodule MirageWeb.Live.NoteListParams do
  defstruct [
    :limit,
    :list,
    :query
  ]
end
