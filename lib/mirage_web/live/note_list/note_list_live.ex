defmodule MirageWeb.NoteListLive do
  use MirageWeb, :live_view
  alias MirageWeb.Live.ListLive

  def mount(_params, _info, socket) do
    notes = list_notes()

    {:ok,
     socket
     |> assign(%{
       notes: notes,
       results: Enum.count(notes),
       changeset: ListLive.note_changeset(),
       lists: ListLive.lists(),
       limit: ListLive.limit(),
       query: ""
     })}
  end

  def list_notes(params \\ %{}) do
    params
    |> parse_params()
    |> Mirage.Notes.list_notes()
    |> Enum.sort_by(fn n -> n.updated_at end, {:desc, NaiveDateTime})
  end

  def handle_event("handle_change", %{"note_list_params" => params}, socket) do
    notes = list_notes(params)

    {:noreply,
     socket
     |> assign(
       notes: notes,
       results: Enum.count(notes),
       changeset: ListLive.note_changeset(params)
     )}
  end

  def parse_params(params) do
    default_params = %{
      preload: true,
      distinct: true
    }

    if Enum.empty?(params) do
      default_params
    else
      new_params =
        Map.merge(default_params, %{
          limit: String.to_integer(params["limit"]),
          search: params["query"]
        })

      new_params =
        if params["list"] == "all" do
          new_params
        else
          Map.put_new(new_params, :list, params["list"])
        end

      new_params
    end
  end
end
