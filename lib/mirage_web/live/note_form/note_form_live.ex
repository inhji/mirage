defmodule MirageWeb.Live.NoteFormLive do
  require Logger
  use MirageWeb, :live_view

  def mount(_params, info, socket) do
    user = Mirage.Accounts.get_user()
    action = info["action"]
    note = info["note"]

    Logger.info("Mounting note_form with action #{action}")

    [changeset, note, tags, targets] = get_changeset_from_action(action, note)

    {:ok,
     socket
     |> assign(
       action: info["action"],
       lists: info["lists"],
       targets: info["targets"],
       selected_targets: targets,
       changeset: changeset,
       tag_string: Enum.map_join(tags, ", ", fn tag -> tag.title end),
       note: note,
       user: user,
       excerpt_length: get_excerpt_length(note.excerpt)
     )}
  end

  def handle_event("handle_validate", %{"note" => note_params}, socket) do
    IO.inspect("Validating")
    IO.inspect(note_params)

    note = socket.assigns.note || %Mirage.Notes.Note{}
    changeset = Mirage.Notes.change_note(note, note_params) |> IO.inspect()
    excerpt_length = get_excerpt_length(changeset.changes.excerpt)

    {:noreply,
     socket
     |> assign(
       changeset: changeset,
       excerpt_length: excerpt_length
     )}
  end

  def handle_event("handle_submit", %{"note" => note_params}, socket) do
    # IO.inspect("Submitting")
    # IO.inspect(note_params)

    case socket.assigns.action do
      "new" ->
        handle_create_note(socket, note_params)

      "edit" ->
        handle_update_note(socket, socket.assigns.note, note_params)
    end
  end

  def handle_create_note(socket, note_params) do
    case Mirage.Notes.create_note_with_hooks(note_params) do
      {:ok, changeset} ->
        {:noreply, redirect(socket, to: Routes.note_path(MirageWeb.Endpoint, :show, changeset))}

      {:error, changeset} ->
        IO.inspect("Error while creating note!")
        IO.inspect(changeset)
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def handle_update_note(socket, note, note_params) do
    case Mirage.Notes.update_note_with_hooks(note, note_params) do
      {:ok, changeset} ->
        {:noreply, redirect(socket, to: Routes.note_path(MirageWeb.Endpoint, :show, changeset))}

      {:error, changeset} ->
        IO.inspect("Error while updating note!")
        IO.inspect(changeset)
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  def form_changeset(attrs \\ %{}) do
    params = %MirageWeb.Live.NoteFormarams{}
    types = %{}

    {params, types}
    |> Ecto.Changeset.cast(attrs, Map.keys(types))
  end

  def targets_to_list(targets) do
    Enum.map(targets, fn target -> target.type end)
  end

  defp get_excerpt_length(excerpt) do
    String.length(excerpt || "")
  end

  defp get_changeset_from_action(action, note) do
    case action do
      "new" ->
        [Mirage.Notes.change_note(%Mirage.Notes.Note{}, %{}), nil, [], []]

      "edit" ->
        Logger.info("Note id: #{note.id}")
        [Mirage.Notes.change_note(note, %{}), note, note.tags, note.syndications]
    end
  end
end
