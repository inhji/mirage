defmodule MirageWeb.API.V1.NoteController do
  use MirageWeb, :controller

  alias Mirage.Notes

  def search(conn, %{"query" => query} = _params) do
    notes = Notes.search_notes(query)

    json(conn, %{
      ok: true,
      data: notes
    })
  end

  def tags(conn, %{"query" => query} = _params) do
    tags = Mirage.Tags.search_tags(query)
    json(conn, tags)
  end

  def tags(conn, _params) do
    tags = Mirage.Tags.search_tags()
    json(conn, tags)
  end
end
