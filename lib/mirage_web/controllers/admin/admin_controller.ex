defmodule MirageWeb.Admin.AdminController do
  use MirageWeb, :controller

  def index(conn, _params) do
    logs = Mirage.Logger.list_logs(limit: 5)

    render(conn, "index.html", page_title: "Admin", logs: logs)
  end

  def logs(conn, _params) do
    logs = Mirage.Logger.list_logs(limit: 10)
    render(conn, "logs.html", page_title: "Logs", logs: logs)
  end
end
