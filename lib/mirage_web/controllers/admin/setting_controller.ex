defmodule MirageWeb.Admin.SettingController do
  use MirageWeb, :controller

  alias Mirage.Settings
  alias Mirage.Settings.Setting

  plug :assign_name_list when action in [:new, :create, :edit, :update]

  def index(conn, _params) do
    settings = Settings.list_settings()
    render(conn, "index.html", settings: settings, page_title: "Settings")
  end

  def new(conn, _params) do
    changeset = Settings.change_setting(%Setting{})
    render(conn, "new.html", changeset: changeset, page_title: "New Setting")
  end

  def create(conn, %{"setting" => setting_params}) do
    case Settings.create_setting(setting_params) do
      {:ok, setting} ->
        conn
        |> put_flash(:info, "Setting created successfully.")
        |> redirect(to: Routes.admin_setting_path(conn, :show, setting))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, page_title: "New Setting")
    end
  end

  def show(conn, %{"id" => id}) do
    setting = Settings.get_setting!(id)
    render(conn, "show.html", setting: setting, page_title: setting.name)
  end

  def edit(conn, %{"id" => id}) do
    setting = Settings.get_setting!(id)
    changeset = Settings.change_setting(setting)
    render(conn, "edit.html", setting: setting, changeset: changeset, page_title: "Edit Setting")
  end

  def update(conn, %{"id" => id, "setting" => setting_params}) do
    setting = Settings.get_setting!(id)

    case Settings.update_setting(setting, setting_params) do
      {:ok, setting} ->
        conn
        |> put_flash(:info, "Setting updated successfully.")
        |> redirect(to: Routes.admin_setting_path(conn, :show, setting))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html",
          setting: setting,
          changeset: changeset,
          page_title: "Edit Setting"
        )
    end
  end

  def delete(conn, %{"id" => id}) do
    setting = Settings.get_setting!(id)
    {:ok, _setting} = Settings.delete_setting(setting)

    conn
    |> put_flash(:info, "Setting deleted successfully.")
    |> redirect(to: Routes.admin_setting_path(conn, :index))
  end

  defp assign_name_list(conn, _opts) do
    conn
    |> assign(:name_list, Mirage.Settings.Setting.allowed_settings())
  end
end
