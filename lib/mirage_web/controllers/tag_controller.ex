defmodule MirageWeb.TagController do
  use MirageWeb, :controller

  def index(conn, _params) do
    {top_tags, rest_tags} =
      Mirage.Tags.list_tags()
      |> Enum.filter(fn t -> Enum.count(t.notes) > 0 end)
      |> split_tags(5)

    render(conn, "index.html",
      page_title: "Tags",
      top_tags: top_tags,
      rest_tags: rest_tags
    )
  end

  defp split_tags(tags, amount \\ 5) do
    sorted_tags =
      tags
      |> Enum.map(fn t -> {t.title, t.slug, Enum.count(t.notes)} end)
      |> Enum.sort_by(fn {n, s, c} -> c end, :desc)

    top_tags = Enum.take(sorted_tags, amount)
    rest_tags = Enum.drop(sorted_tags, amount)

    {top_tags, rest_tags}
  end

  def show(conn, %{"id" => id}) do
    tag = Mirage.Tags.get_tag!(id)

    render(conn, "show.html",
      page_title: "Tagged with “#{tag.title}”",
      tag: tag,
      preview: true
    )
  end
end
