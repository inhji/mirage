defmodule MirageWeb.SearchController do
  use MirageWeb, :controller

  def index(conn, %{"for" => query}) do
    notes =
      query
      |> Mirage.Notes.search_notes()
      |> Enum.sort_by(fn {_type, n} -> n.updated_at end, :desc)
      |> Enum.uniq_by(fn {_type, n} -> n.title end)

    render(conn, "index.html",
      query: query,
      notes: notes,
      page_title: "Search"
    )
  end

  def index(conn, _params) do
    render(conn, "index.html",
      query: nil,
      notes: nil,
      page_title: "Search"
    )
  end
end
