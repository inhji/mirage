defmodule MirageWeb.SiteInfo do
  import Plug.Conn, only: [assign: 3]

  @doc """
  Assigns the pages of the site to the conn-object.
  Returns an empty list if no pages are found.
  """
  def fetch_pages(conn, _opts) do
    pages =
      case Mirage.Accounts.get_user() do
        nil ->
          []

        user ->
          Mirage.Notes.list_notes(
            published: true,
            list: user.page_list_id
          )
      end

    assign(conn, :published_pages, pages)
  end

  @doc """
  Assigns the pages of the site to the conn-object.
  Returns an empty list if no pages are found.
  """
  def fetch_lists(conn, _opts) do
    lists =
      case Mirage.Accounts.get_user() do
        nil -> []
        _user -> Mirage.Lists.list_published_lists()
      end

    assign(conn, :published_lists, lists)
  end

  def fetch_settings(conn, _opts) do
    conn
    |> fetch_setting("custom_css")
    |> fetch_setting("custom_html")
    |> fetch_setting("motd", &get_random_motd/1)
  end

  defp fetch_setting(conn, name, transform_fn \\ & &1) do
    setting = Mirage.Settings.get_setting(name)

    value =
      cond do
        is_nil(setting) -> nil
        is_nil(transform_fn) -> setting.value
        true -> transform_fn.(setting.value)
      end

    assign(conn, String.to_atom(name), value)
  end

  defp get_random_motd(nil), do: ""
  defp get_random_motd(""), do: ""

  defp get_random_motd(motd_string) do
    motd_string
    |> String.split("\n")
    |> Enum.take_random(1)
    |> List.first()
  end
end
