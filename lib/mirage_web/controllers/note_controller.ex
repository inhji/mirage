defmodule MirageWeb.NoteController do
  use MirageWeb, :controller

  def index_bookmark(conn, _params) do
    user = Mirage.Accounts.get_user()

    bookmarks =
      Mirage.Notes.list_notes(
        published: true,
        list: user.bookmark_list_id,
        preload: true
      )
      |> group_by_month()

    render(conn, "lists/bookmarks.html",
      notes: bookmarks,
      page_title: "Bookmarks",
      preview: true
    )
  end

  def index_article(conn, _params) do
    user = Mirage.Accounts.get_user()

    bookmarks =
      Mirage.Notes.list_notes(
        published: true,
        list: user.article_list_id,
        preload: true
      )
      |> group_by_month()

    render(conn, "lists/articles.html",
      notes: bookmarks,
      page_title: "Bookmarks",
      preview: true
    )
  end

  def show(conn, %{"id" => id}) do
    note = Mirage.Notes.get_note!(id)

    render(conn, "show.html",
      note: note,
      page_title: note.title,
      preview: false
    )
  end

  def group_by_month(notes) do
    notes
    |> Enum.group_by(fn note ->
      Timex.set(note.published_at,
        day: 1,
        hour: 0,
        minute: 0,
        second: 0
      )
    end)
    |> Enum.sort_by(fn {date, _notes} -> {date} end, :desc)
    |> Enum.map(fn {date, notes} ->
      {date,
       Enum.sort_by(
         notes,
         fn note ->
           note.published_at
         end,
         :desc
       )}
    end)
  end
end
