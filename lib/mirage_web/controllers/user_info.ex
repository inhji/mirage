defmodule MirageWeb.UserInfo do
  import Plug.Conn

  alias Mirage.{Accounts, Identities}

  @token_endpoint Application.compile_env!(:mirage, [:indie, :token_endpoint])
  @auth_endpoint Application.compile_env!(:mirage, [:indie, :auth_endpoint])

  @doc """
  Assigns the identities of the user to the conn-object.
  Returns an empty list if no user is yet defined.
  """
  def fetch_user_identities(conn, _opts) do
    identities =
      case Accounts.get_user() do
        nil ->
          []

        user ->
          Identities.list_user_identities(user)
      end

    assign(conn, :identities, identities)
  end

  def fetch_indie_config(conn, _opts) do
    assign(conn, :indie_config, %{
      token_endpoint: @token_endpoint,
      auth_endpoint: @auth_endpoint,
      micropub_endpoint: "/indie/micropub"
    })
  end
end
