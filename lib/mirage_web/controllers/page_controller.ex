defmodule MirageWeb.PageController do
  use MirageWeb, :controller

  def index(conn, _params) do
    user = Mirage.Accounts.get_user()
    stream = list_updates(user)

    render(conn, "index.html",
      stream: stream,
      user: user,
      preview: true,
      page_title: "Home"
    )
  end

  def list_updates(nil), do: []

  def list_updates(user) do
    [
      limit: 50,
      published: true,
      list: [
        user.microblog_list_id,
        user.bookmark_list_id,
        user.wiki_list_id
      ],
      preload: true,
      order: true
    ]
    |> Mirage.Notes.list_notes()
    |> Enum.sort_by(fn n -> n.published_at end, {:desc, NaiveDateTime})
    |> Enum.chunk_by(fn n -> n.list_id end)
    |> Enum.reduce([], fn list, acc ->
      list_id =
        list
        |> List.first()
        |> Map.get(:list_id)

      entry =
        cond do
          list_id == user.bookmark_list_id ->
            # Bookmarks are supposed to be grouped together,
            # so we just return the whole list. its up to the
            # template how to render this list
            [{:bookmark, list}]

          list_id == user.wiki_list_id ->
            # Wiki articles follow the same rule as bookmarks
            [{:wiki, list}]

          true ->
            # normal notes aka updates are just wrapped in the tuple
            # and marked as a note
            Enum.map(list, fn note -> {:note, note} end)
        end

      acc ++ entry
    end)
  end
end
