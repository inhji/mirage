defmodule MirageWeb.ViewHelpers do
  alias Mirage.Notes.Note

  def datetime_for_display(datetime) do
    Timex.format!(datetime, "{D}. {Mfull} {YYYY}")
  end

  def datetime_for_article(datetime) do
    Timex.format!(datetime, "{0D}. {Mshort}")
  end

  def datetime_from_now(nil), do: nil
  def datetime_from_now(datetime), do: Timex.from_now(datetime)

  def note?(%Mirage.Notes.Note{}), do: true
  def note?(_), do: false

  def microblog?(%Mirage.Notes.Note{} = note), do: Mirage.Notes.Note.has_datetitle?(note)
  def microblog?(_), do: false

  def bookmark?(%Note{url: url, url_type: "bookmark_of"}) when is_binary(url), do: true
  def bookmark?(_), do: false

  def like?(%Note{url: url, url_type: "like_of"}) when is_binary(url), do: true
  def like?(_), do: false

  def reply?(%Note{url: url, url_type: "reply_of"}) when is_binary(url), do: true
  def reply?(_), do: false

  def emoji_bool(bool) do
    case bool do
      true -> "✅"
      false -> "⭕️"
    end
  end
end
