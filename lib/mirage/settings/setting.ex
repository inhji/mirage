defmodule Mirage.Settings.Setting do
  use Ecto.Schema
  import Ecto.Changeset

  @allowed_settings ["custom_css", "custom_html", "motd"]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "settings" do
    field :name, :string
    field :value, :string
    field :notes, :string

    timestamps()
  end

  def allowed_settings do
    @allowed_settings
  end

  @doc false
  def changeset(setting, attrs) do
    setting
    |> cast(attrs, [:name, :value, :notes])
    |> validate_required([:name, :value])
    |> validate_inclusion(:name, @allowed_settings)
    |> unique_constraint(:name)
  end
end
