defmodule Mirage.Uploads.Upload do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "uploads" do
    field :image_filename, Mirage.Uploads.ImageUploader.Type
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(upload, attrs) do
    upload
    |> cast(attrs, [:title, :image_filename])
  end
end
