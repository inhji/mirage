defmodule Mirage.Uploads.ImageUploader do
  use Waffle.Definition
  use Waffle.Ecto.Definition

  @versions [:original, :full]

  # Whitelist file extensions:
  def validate({file, _}) do
    ~w(.jpg .jpeg .gif .png)
    |> Enum.member?(Path.extname(file.file_name) |> String.downcase())
  end

  def transform(:full, _) do
    {:convert,
     "-thumbnail 1280x1024^ -gravity center -extent 1280x1024 -colors 24 -dither FloydSteinberg -format png",
     :png}
  end

  def transform(:thumb, _) do
    {:convert,
     "-thumbnail 150x150^ -gravity center -extent 150x150 -colors 24 -dither FloydSteinberg -format png",
     :png}
  end

  # Override the persisted filenames:
  def filename(version, {file, scope}) do
    IO.inspect(file)
    slug = "slug"
    # slug = Slugger.slugify_downcase(scope.id)
    "#{slug}/#{version}"
  end

  # Override the storage directory:
  def storage_dir(_version, {_file, _scope}) do
    "uploads/files/images"
  end
end
