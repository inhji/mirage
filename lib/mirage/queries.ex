defmodule Mirage.Queries do
  @moduledoc """
  Adds constraints to a note query:

  - order_by
  - limit
  - search term
  - published
  - list
  """

  import Ecto.Query, warn: false
  import Mirage.Macros
  alias Mirage.Lists.List
  alias Mirage.Notes.NoteTag
  alias Mirage.Tags.Tag

  def where_published(query), do: where(query, [n], not is_nil(n.published_at))

  def where_unpublished(query), do: where(query, [n], is_nil(n.published_at))

  def where_in_list(query, nil), do: query

  def where_in_list(query, list_ids) when is_list(list_ids),
    do:
      query
      |> join(:inner, [n], l in List, on: [id: n.list_id])
      |> where([n], n.list_id in ^list_ids)

  def where_in_list(query, list_id),
    do:
      query
      |> join(:inner, [n], l in List, on: [id: n.list_id])
      |> where([n], n.list_id == ^list_id)

  def where_contains(query, ""), do: query

  def where_contains(query, search_string),
    do:
      query
      |> join(:left, [n], l in List, on: [id: n.list_id])
      |> join(:left, [n, l], nt in NoteTag, on: [note_id: n.id])
      |> where([n], contains(n.content, ^search_string))
      |> or_where([n], contains(n.title, ^search_string))
      |> or_where([n, l], contains(l.title, ^search_string))

  def where_contains2(query, ""), do: query

  def where_contains2(query, search_string) do
    content_query =
      query
      |> where([n], contains(n.content, ^search_string))
      |> select([n], {"content", n})

    title_query =
      query
      |> where([n], contains(n.title, ^search_string))
      |> select([n], {"title", n})

    list_query =
      query
      |> join(:inner, [n], l in List, on: [id: n.list_id])
      |> where([n, l], contains(l.title, ^search_string))
      |> select([n], {"list", n})

    tag_query =
      query
      |> join(:inner, [n], nt in NoteTag, on: [note_id: n.id])
      |> join(:inner, [n, nt], t in Tag, on: [id: nt.tag_id])
      |> where([n, nt, t], contains(t.title, ^search_string))
      |> select([n], {"tag", n})

    content_query
    |> union(^title_query)
    |> union(^list_query)
    |> union(^tag_query)
  end
end
