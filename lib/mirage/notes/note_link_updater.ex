defmodule Mirage.Notes.NoteLinkUpdater do
  require Logger

  def add_note_links(origin_note, slugs) do
    Enum.each(slugs, fn slug ->
      case linked_note = Mirage.Notes.get_note(slug) do
        nil ->
          Logger.warn("Reference to '#{slug}' could not be resolved")

        note ->
          add_note_link(slug, origin_note, linked_note)
      end
    end)
  end

  def add_note_link(slug, origin_note, linked_note) do
    with attrs <- get_attrs(origin_note.id, linked_note.id),
         {:ok, _note_note} <- Mirage.NoteNotes.create_note_note(attrs) do
      Logger.info("Reference to '#{slug}' created")
    else
      error ->
        Logger.warn(error)
    end
  end

  def remove_note_links(origin_note, slugs) do
    Enum.each(slugs, fn slug ->
      linked_note = Mirage.Notes.get_note!(slug)
      attrs = get_attrs(origin_note.id, linked_note.id)
      note_note = Mirage.NoteNotes.get_note_note(attrs)

      case Mirage.NoteNotes.delete_note_note(note_note) do
        {:ok, _note_note} ->
          Logger.info("Reference to '#{slug}' deleted")

        error ->
          Logger.warn(error)
      end
    end)
  end

  defp get_attrs(origin_id, linked_id) do
    %{
      source_id: origin_id,
      target_id: linked_id
    }
  end
end
