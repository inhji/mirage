defmodule Mirage.References do
  @moduledoc """
  Finds and replaces references to entities in a string.

  A reference is an internal link like the following examples:

  * `[[sustainablity]]` -> A note named *Sustainability*
  * `[[tag:video-games]]` -> A tag named *Video Games*
  * `[[list:blog]]` -> A list named *Blog*
  * `[[a-long-unfitting-title|A simple title]]` -> A note named *A long unfitting title*
  """

  alias MirageWeb.Router.Helpers, as: Routes

  @reference_regex ~r/\[\[(?:(?<type>list|tag):)?(?<id>[\d\w-]+)(?:\|(?<title>[\w\d\s'`äöüß]+))?\]\]/

  @note_type "note"
  @tag_type "tag"
  @list_type "list"

  @doc """
  Returns a list of references in `string`.
  """
  def get_references(nil), do: []

  def get_references(string) do
    @reference_regex
    |> Regex.scan(string, capture: :all)
    |> Enum.map(&map_to_tuple/1)
  end

  @doc """
  Checks each reference returned from `get_references/1` and validates its existence.
  """
  def validate_references(references) do
    Enum.map(references, fn {placeholder, type, slug, custom_title} ->
      schema =
        case type do
          @tag_type -> Mirage.Tags.get_tag(slug)
          @list_type -> Mirage.Lists.get_list(slug)
          _ -> Mirage.Notes.get_note(slug)
        end

      valid =
        case schema do
          nil -> false
          _ -> true
        end

      # If a custom title was defined, use it, 
      # otherwise use the notes/tag/lists title
      title =
        if slug == custom_title do
          schema.title
        else
          custom_title
        end

      {placeholder, type, slug, title, valid}
    end)
  end

  @doc """
  Returns a list of slugs that are referenced in `string`, optionally filtering by `filter_type`.
  """
  def get_reference_ids(string, filter_type \\ @note_type) do
    string
    |> get_references()
    |> Enum.filter(fn {_, type, _, _} = _ref -> type == filter_type end)
    |> Enum.map(fn {_, _, note_slug, _} = _ref -> note_slug end)
  end

  @doc """
  Finds and replaces references with the matching url in `string`.
  """
  def replace_references(string) do
    string
    |> get_references()
    |> validate_references()
    |> Enum.reduce(string, fn {placeholder, type, slug, title, valid}, s ->
      String.replace(s, placeholder, get_link(type, slug, title, valid))
    end)
  end

  defp get_link(type, slug, title, valid \\ true) do
    path =
      case type do
        @tag_type ->
          Routes.tag_path(MirageWeb.Endpoint, :show, slug)

        @list_type ->
          Routes.list_path(MirageWeb.Endpoint, :show, slug)

        _ ->
          Routes.note_path(MirageWeb.Endpoint, :show, slug)
      end

    do_get_link(title, path, valid)
  end

  defp do_get_link(title, path, valid) do
    "[#{title}](#{path})#{get_link_class(valid)}"
  end

  defp get_link_class(valid) do
    case valid do
      false -> "{:.invalid}"
      _ -> ""
    end
  end

  defp map_to_tuple([placeholder, type, note_slug]),
    do: {placeholder, get_reference_type(type), note_slug, note_slug}

  defp map_to_tuple([placeholder, type, note_slug, title]),
    do: {placeholder, get_reference_type(type), note_slug, title}

  defp get_reference_type(nil), do: @note_type
  defp get_reference_type(""), do: @note_type
  defp get_reference_type(type), do: type
end
